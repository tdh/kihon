<?php
	// archive.php handles all the archives

	// Include header.php
	get_header();
?>

<section id="content-container">

	<header class="archive-header">
	    <h1 class="archive-title">
	    <?php 
	    	// Daily archives
	    	if ( is_day() ) : 
		        printf( _( 'Daily Archives: %s', 'kihon_theme'), '<span>' . get_the_date() . '</span>' );
		    
		    // Monthly archives
		    elseif ( is_month() ) :
	        	printf( _( 'Monthly Archives: %s', 'kihon_theme'), '<span>' . get_the_date( 'F Y', 'monthly archives date format' ) . '</span>' );
			
			// Yearly archives
			elseif ( is_year() ) :
	        	printf( _( 'Yearly Archives: %s', 'kihon_theme'), '<span>' . get_the_date( 'Y', 'yearly archives date format' ) . '</span>' );
	        
	        // Category archives
	        elseif ( is_category() ) :
	        	single_cat_title();
	        	
	        // Tag archives
	        elseif ( is_tag() ) :
	        	single_tag_title();
	        
	        // Custom taxonomies archives
	        elseif ( is_tax() ) :
	        	single_term_title();
	    
	    	// Everything else
	    	else :
	        	_e( 'Archives', 'kihon_theme');
	    	
	    	endif; 
	    ?>
	    </h1>
	</header>

<?php 
	// The basic loop
	while ( have_posts() ) : the_post(); 
	
	// Load the appropriate content template
	get_template_part( 'content', 'archive' );
	
	// End the loop
	endwhile;
	
	// Navigation top
	get_template_part( 'nav', 'bottom' );
?>
				
</section>

<?php 
	// Include sidebar.php
	get_sidebar();

	// Include header.php
	get_footer(); 
?>