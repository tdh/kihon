<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<h1 class="entry-title page-title">
    		<?php the_title(); ?>
		</h1>
    </header>
    <div class="entry-content">
        <?php the_content(); ?>
        <?php wp_link_pages( array( 
            'before' => '<div class="page-link">' . __( 'Pages:', 'kihon_theme'), 
            'after' => '</div>' 
            )
        ); ?>
    </div>
</article>