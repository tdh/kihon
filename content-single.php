<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<h1 class="entry-title">
    		<?php the_title(); ?>
		</h1>
        <p class="author-meta">
            By <span><?php the_author_posts_link(); ?></span>
        </p>
    </header>
    <div class="entry-content">
        <?php the_content(); ?>
        <?php wp_link_pages( array( 
            'before' => '<div class="page-link">' . __('Pages:', 'kihon_theme'), 
            'after' => '</div>' 
            )
        ); ?>
    </div>
    <footer class="entry-meta">
    <?php
        // Is there any author bio?
        if ( get_the_author_meta( 'description' ) ) {
    ?>

        <div class="author-bio">
            <?php echo get_avatar( get_the_author_meta('ID'), 48 ); ?>
            <h3 class="author-title"><?php the_author_posts_link(); ?></h3>
            <p class="author-description"><?php echo get_the_author_meta( 'description' ); ?></p>
        </div>

    <?php
        } // All done
    ?>
    	<p class="entry-postmeta">
    	<?php
    		// Date and time
    		_e( 'Published ', 'kihon_theme') ; the_date(); 
    	
    		// Categories
    		_e( ' and filed in ', 'kihon_theme') ; the_category( ', ' ); 
    		
    		// Tags
    		the_tags( '<br /><span class="meta-tag-list">' . __('Tagged with ', 'kihon_theme'), ', ', '</span>' );
    	?>
    	</p>
    </footer>
    <?php comments_template( '', true ); ?>
</article>