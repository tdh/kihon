<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php if ( is_singular() ) { ?>
	<header class="entry-header">
		<h1 class="entry-title">
    		<?php the_title(); ?>
		</h1>
	<?php } else { ?> 
	    <h2 class="entry-title">
    		<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" rel="bookmark">
    			<?php the_title(); ?>
	    	</a>
    	</h2>
    <?php } ?>
    </header>
    <?php
    	// For archives and search results, use the_excerpt()
    	if ( is_home() || is_front_page() || is_archive() || is_search() ) : ?>
    	<div class="entry-summary">
    	    <?php the_excerpt(); ?>
    	    <div class="read-more">
   		    	<?php
   		    	   // If the comments are open we'll need the comments template
   		    	   if (comments_open()) { ?>
   		    	   	<span class="comments-link">
   		    	   		<?php comments_popup_link( __( 'Leave a comment', 'kihon_theme'), __( '1 comment', 'kihon_theme') , __( '% comments', 'kihon_theme') ); ?>
   		    	   	</span> &bull;  
   		    	<?php } ?>
    	    	<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" rel="bookmark">
    	    		<?php _e('Continue reading &rarr;', 'kihon_theme'); ?>
    	    	</a>
    	    </div>
    	</div>
    <?php 
    	// For everything else
    	else : ?>
    	<div class="entry-content">
    	    <?php the_content(); ?>
    	    <?php wp_link_pages( array( 
                'before' => '<div class="page-link">' . __('Pages: ', 'kihon_theme'), 
                'after' => '</div>' 
                ) 
            ); ?>
    	</div>
    <?php endif; 
    	// Comments only on single posts
    	if ( is_singular() ) {
    		comments_template( '', true ); 
    	} 
    ?>
</article>