				<footer id="footer-container">
					<p class="footer-copy">
						<?php echo get_theme_mod( 'kihon_footer_copyright' ); ?>
					</p>
					<p class="kihon-credits" <?php if ( get_theme_mod( 'kihon_footer_credits' ) == 1 ) { echo 'style="display: none;"'; } ?>>
						<a href="<?php echo home_url(); ?>" title="<?php bloginfo( 'title'); ?>">
							<?php printf( __(	'%s is powered by %s and %s by %s', 'kihon_theme'), 
												'<a href="' . get_bloginfo( 'url') .' ">' . get_bloginfo( 'title') .  '</a>', 
												'<a href="http://wordpress.org" title="WordPress">WordPress</a>',
												'<a href="http://tdh.me/wordpress/kihon/" title="Kihon">Kihon</a>',
												'<a href="http://tdh.me" title="TDH">TDH</a>'
							) ?>
					</p>
				</footer>

			</div><!-- #inner-wrap -->
		</div><!-- #outer-wrap -->

		<?php wp_footer(); ?>

	</body>

</html>