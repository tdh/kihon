<?php

// THEME SETUP
function kihon_setup() {

	// Add RSS feed links to <head>
	add_theme_support( 'automatic-feed-links' );

	// Add custom header support
	$args = array(
		'flex-width'	=> true,
		'width'	=> 860,
		'flex-height'	=> true,
		'height'	=> 120,
	);
	add_theme_support( 'custom-header', $args );
	
	// Add custom background support
	add_custom_background();

	// Register menus
	register_nav_menu( 'main-menu', __( 'Main Menu', 'kihon_theme' ) );
    
    // Internationalization
    load_theme_textdomain( 'kihon_theme', get_template_directory() . '/languages' );

}
add_action( 'after_setup_theme', 'kihon_setup' );

// REGISTER SIDEBARS
function kihon_widgets_init() {

	// Primary right column
	register_sidebar( array(
	    'name' => __('Primary Right Column', 'kihon_theme'),
	    'id' => 'primary-right-column',
	    'description' => __('The primary right column.', 'kihon_theme'),
	    'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
	    'after_widget' => '</li>',
	    'before_title' => '<h3 class="widget-title">',
	    'after_title' => '</h3>'
	) );

}
add_action( 'widgets_init', 'kihon_widgets_init' );

// THEME SETTINGS
function kihon_theme_settings( $wp_customize ) {

    // Add kihon_footer section footer settings
    $wp_customize->add_section( 'kihon_footer', array(
        'title' => __('Footer Details', 'kihon_theme'), // Title of section
        'description' => __('Customize your site\'s footer copy.', 'kihon_theme'), // Description of section
    ) );
 
    // Add setting for kihon_footer_copyright
    $wp_customize->add_setting( 'kihon_footer_copyright', array(
    	'default' => '&copy; ' . get_bloginfo( 'name' ),
	) );

	// Add control for kihon_footer_copyright
	$wp_customize->add_control( 'kihon_footer_copyright', array(
	    'label' => __('Text in the footer', 'kihon_theme'),
	    'type' => 'text', 
	    'section' => 'kihon_footer', // Add to section kihon_footer
	) );

	// Add setting for kihon_footer_credits
	$wp_customize->add_setting( 'kihon_footer_credits', array(
	    'default' => 0,
	) );
	 
	// Add control for kihon_footer_credits
	$wp_customize->add_control( 'kihon_footer_credits', array(
	    'label' => __('Hide credits from the footer', 'kihon_theme'),
	    'type' => 'checkbox',
	    'section' => 'kihon_footer', // Add to section kihon_footer
	) );

}
add_action( 'customize_register', 'kihon_theme_settings', 11 );

// KIHON LOGO ON WP-LOGIN.PHP
// Swap the logo
function kihon_login_logo() {
    
    // Overwrite the current logo with CSS
    echo '<style type="text/css">
          #login h1 a { 
          	background-image:url(' . get_template_directory_uri() . '/img/kihon-logo.png) !important;
          	background-size: 132px 107px;
          	width: 132px;
          	height: 107px;
          	margin: 0 auto;
          }
          </style>';

}
add_action( 'login_head', 'kihon_login_logo' );

// Change the logo URL
function kihon_login_logo_url() {
	return get_bloginfo( 'url' );
}
add_filter( 'login_headerurl', 'kihon_login_logo_url' );

// Change the logo title
function kihon_login_logo_url_title() {
    return get_bloginfo( 'description' );
}
add_filter( 'login_headertitle', 'kihon_login_logo_url_title' );
// --ends

// COMMENT CALLBACK
function kihon_comment($comment, $args, $depth) {
   $GLOBALS['comment'] = $comment; ?>
   <li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
     <div id="comment-<?php comment_ID(); ?>" class="comment-body">
      <div class="comment-author vcard">
         <?php echo get_avatar($comment,$size='32',$default='<path_to_url>' ); ?>
         <?php printf( __('<cite class="fn">%s</cite> <span class="says">says:</span>', 'kihon_theme'), get_comment_author_link()) ?>
      </div>
      <?php if ($comment->comment_approved == '0') : ?>
         <em><?php _e('Your comment is awaiting moderation.', 'kihon_theme') ?></em>
         <br />
      <?php endif; ?>

      <div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>"><?php printf(__('%1$s at %2$s', 'kihon_theme'), get_comment_date(),  get_comment_time()) ?></a><?php edit_comment_link(__('(Edit)', 'kihon_theme'),'  ','') ?></div>

      <?php comment_text() ?>

      <div class="reply">
         <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
      </div>
     </div>
<?php }

?>