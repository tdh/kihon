<!DOCTYPE html>
<html <?php language_attributes(); ?>>

	<head>
		<meta charset="utf-8">
		<meta name="description" content="<?php bloginfo( 'description' ); ?>" />
		<meta name="author" content="<?php bloginfo( 'name' ); ?>">
		<meta name="viewport" content="initial-scale = 1,user-scalable=no,maximum-scale=1.0">
		<meta name="HandheldFriendly" content="true"/> 

		<title>
		<?php
			// Based on Twenty Eleven
			global $page, $paged;
		
			wp_title( '|', true, 'right' );
		
			// Add the blog name.
			bloginfo( 'name' );
		
			// Add the blog description for the home/front page.
			$site_description = get_bloginfo( 'description', 'display' );
			if ( $site_description && ( is_home() || is_front_page() ) )
				echo " | $site_description";
		
			// Add a page number if necessary:
			if ( $paged >= 2 || $page >= 2 )
				echo ' | ' . sprintf( 'Page %s', max( $paged, $page ) );
		?>
		</title>

		<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
		
		<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		
		<?php 
			// Queue threaded comment JavaScript
			if ( is_singular() && get_option( 'thread_comments' ) )
				wp_enqueue_script( 'comment-reply' );
			
			// Kick off WordPress
			wp_head();
		?>
		
	</head>

	<body <?php body_class(); ?>>

		<div id="outer-wrap">
			<div id="inner-wrap">

				<header id="header-container">
				<?php 
					// Is there a header image?
					if ( get_header_image() != '' ) {
						
						// Is there room for site description?
						if ( get_custom_header()->width < 550 ) {
				?>

					<div id="site-description">
						<p style="line-height: <?php echo get_custom_header()->height; ?>px;"><?php bloginfo( 'description'); ?></p>
					</div>
				
				<?php } // Now for the actual header image ?>

					<h1 id="site-header">
						<a href="<?php echo home_url(); ?>" title="<?php bloginfo( 'title' ) ;?>">
							<img src="<?php header_image(); ?>" height="<?php echo get_custom_header()->height; ?>" width="<?php echo get_custom_header()->width; ?>" alt="<?php bloginfo( 'title' ) ;?>" />
						</a>
					</h1>

				<?php } 
					// No header image, output text
					else { 
				?>

					<div id="site-description">
						<p><?php bloginfo( 'description'); ?></p>
					</div>
					<h1 id="site-title">
						<a href="<?php echo site_url(); ?>" title="<?php bloginfo( 'title' ) ;?>">
							<?php bloginfo( 'title' ); ?>
						</a>
					</h1>

				<?php } // Done with header image ?>

					<nav id="header-navigation">
						<?php 
							// Main navigation
							wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); 
						?>
					</nav>
				</header>