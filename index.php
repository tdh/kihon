<?php
	// index.php is the fallback template

	// Include header.php
	get_header();
?>

<section id="content-container">

<?php 
	// 404 Page Not Found or empty archives etc.
	if ( !have_posts() ) : ?>
	<article id="post-0" class="post error404 not-found">
		<h1 class="entry-title">Not Found</h1>
		<div class="entry-content">
			<p><?php _e( 'Sorry, there is nothing here. You might want to try and search for whatever it was you were looking for?', 'kihon_theme' ) ?></p>
			<?php get_search_form(); ?>
		</div>
	</article>
<?php 
	endif;
	
	// The basic loop
	while ( have_posts() ) : the_post(); 
	
	// Load the appropriate content template
	get_template_part( 'content', 'index' );
	
	// End the loop
	endwhile;
	
	// Navigation top
	get_template_part( 'nav', 'bottom' );
?>
				
</section>

<?php 
	// Include sidebar.php
	get_sidebar();

	// Include header.php
	get_footer(); 
?>