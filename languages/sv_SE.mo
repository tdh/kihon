��    "      ,  /   <      �     �  
                    A  	   H  :   R  3   �  0   �     �     �  "        6     I     X     u  	   �     �  3   �  0   �  +   	     5     <  
   Q     \     r  h   �     �            I   1     {  $   �  s  �     (	     ;	     I	     V	  
   s	     ~	  <   �	  7   �	  3   �	     3
     9
     Q
     k
     y
     �
     �
  	   �
     �
  6   �
  2   �
  $   2     W     ^  
   t       #   �  i   �  
   "     -     ;  S   Y     �  $   �                                                    !          
      	                             "                                                              and filed in  % comments %1$s at %2$s %s is powered by %s and %s by %s (Edit) 1 comment <cite class="fn">%s</cite> <span class="says">says:</span> <span class="meta-nav">&larr;</span> Older comments <span class="meta-nav">&larr;</span> Older posts Archives Continue reading &rarr; Customize your site's footer copy. Daily Archives: %s Footer Details Hide credits from the footer Leave a comment Main Menu Monthly Archives: %s Newer comments <span class="meta-nav">&rarr;</span> Newer posts <span class="meta-nav">&rarr;</span> One response to %2$s %1$s responses to %2$s Pages: Primary Right Column Published  Search results for %s Sorry, comments are closed. Sorry, there is nothing here. You might want to try and search for whatever it was you were looking for? Tagged with  Text in the footer The primary right column. This post is password protected. Enter the password to view any comments. Yearly Archives: %s Your comment is awaiting moderation. Project-Id-Version: Kihon v1.4
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-03-30 15:55+0100
PO-Revision-Date: 2015-03-30 15:56+0100
Last-Translator: Thord Daniel Hedengren <tdh@oddalice.se>
Language-Team: 
Language: sv_SE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Poedit 1.7.5
X-Poedit-SourceCharset: utf-8
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Poedit-Basepath: ../
X-Textdomain-Support: yes
X-Poedit-SearchPath-0: .
 och arkiverad som  % kommentarer %1$s av %2$s %s drivs med %s och %s av %s (Redigera) 1 kommentar <cite class="fn">%s</cite> <span class="says">säger:</span> <span class="meta-nav">&larr;</span> Äldre kommentarer <span class="meta-nav">&larr;</span> Äldre inlägg Arkiv Fortsätt läsa  &rarr; Anpassa din sajtes sidfot Dagsarkiv: %s Sidfotstext  Göm text i sidfot Lämna en kommentar Huvudmeny Månadsakriv: %s Nyare kommentarer <span class="meta-nav">&rarr;</span> Nyare inlägg <span class="meta-nav">&rarr;</span> Ett svar på %2$s %1$s svar på %2$s Sidor: Primär kolumn höger Publicerad Sökresultat för %s Tyvärr, kommentarer är stängda.  Tyvärr så finns det inget på den här adressen. Vill du testa att söka efter det du söker istället? Taggat med Text i sidfot Den primära högerkolumnen.  Det här inlägget är lösenordsskyddat. Ange lösenord för att se kommentarerna. Årsarkiv: %s Din kommentar inväntar godkännade. 