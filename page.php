<?php
	// page.php is used for single posts

	// Include header.php
	get_header();
?>

<section id="content-container">

<?php 
	// The basic loop
	while ( have_posts() ) : the_post(); 
	
	// Load the appropriate content template
	get_template_part( 'content', 'page' );
	
	// End the loop
	endwhile;
?>
				
</section>

<?php 
	// Include sidebar.php
	get_sidebar();

	// Include header.php
	get_footer(); 
?>