KIHON: README


# ABOUT KIHON

Kihon is a simple blog theme for WordPress, created by Thord Daniel Hedengren.
The theme is made available free of charge, and is licensed under the GPL license.

Kihon was made for WordPress 3.4 or later, and was featured in Web Designer magazine
issue 203.

The official site for Kihon is: http://tdh.me/wordpress/kihon/


# SUPPORT

Kihon is supplied as is. The theme author takes no responsibilities whatsoever.

If you have any questions, contact Thord Daniel Hedengren using:

	* Twitter [http://twitter.com/tdh]
	* App.net [http://alpha.app.net/tdh]
	* Other   [http://tdh.me/about/]

For help with WordPress installation and other similar issues, please turn to the
official support forums: http://wordpress.org/support/