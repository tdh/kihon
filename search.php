<?php
	// search.php handles searches

	// Include header.php
	get_header();
?>

<section id="content-container">

	<header class="archive-header">
	    <h1 class="archive-title">
	    	<?php printf( __( 'Search results for %s', 'kihon_theme'), '<span>' . get_search_query() . '</span>' ); ?>
	    </h1>
	</header>

<?php 
	// The basic loop
	while ( have_posts() ) : the_post(); 
	
	// Load the appropriate content template
	get_template_part( 'content', 'index' );
	
	// End the loop
	endwhile;
	
	// Navigation top
	get_template_part( 'nav', 'bottom' );
?>
				
</section>

<?php 
	// Include sidebar.php
	get_sidebar();

	// Include header.php
	get_footer(); 
?>