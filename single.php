<?php
    // Include header.php
	get_header();
?>

<section id="content-container">

<?php 
	// The basic loop
	while ( have_posts() ) : the_post(); 
	
	// Load the appropriate content template
	get_template_part( 'content', 'single' );
	
	// End the loop
	endwhile;
	
	// Navigation top
	get_template_part( 'nav', 'bottom' );
?>
				
</section>

<?php 
	// Include sidebar.php
	get_sidebar();

	// Include header.php
	get_footer(); 
?>